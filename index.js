const RoutesConfig = require('./configs/RoutesConfig')
const ExpressConfig = require('./configs/ExpressConfig')
const ThrowErrorConfig = require('./configs/ThrowErrorConfig')
const TokenGenerateConfig = require('./configs/TokenGenerateConfig')

const configs = {
  ExpressConfig,
  RoutesConfig,
  ThrowErrorConfig,
  TokenGenerateConfig
}

module.exports = {configs}
