const log = require('colog')
const moment = require('moment')
const {upperCase} = require('lodash')

const toJson = (val, cache = []) => JSON.stringify(val, (key, value) => {
  if (key === '_search') return
  const isObjectValues = typeof value === 'object' && value !== null

  if (isObjectValues) {
    if (cache.indexOf(value) > -1) return
    cache.push(value)
  }

  return value
})

module.exports = (server) => {
  const {app, routes, execute} = server
  const routesMaps = Object.keys(routes || {}).map(key => routes[key])

  routesMaps.forEach(({verb, path, send, exec}) => {
    const resolveRequest = async (req, res) => {
      res.set('Content-Type', 'application/json')

      try {
        const context = {req, res, exec}
        Object.assign(context, server)
        let result = await execute(context, exec)
        res[send || 'send'](send === 'pdfFromHTML' ? result : toJson(result))
      } catch (err) {
        const error500 = err instanceof Error
        res.statusCode = error500 ? 500 : err.statusCode

        if (error500) {
          log.error('')
          log.error('')
          log.error('')
          log.error('===========================================================')
          log.error(` ${moment().format('DD/MM/YYYY HH:mm:ss')} - Critical error`)
          log.error(`  route..: ${upperCase(verb)} ${path}`)
          log.error(`  body...: ${JSON.stringify(req.body)}`)
          log.error(`  message: ${err.message}`)
          log.error('  trace:')
          console.log(err)
          log.error('===========================================================')
          log.error('')
          log.error('')
          log.error('')
        }

        res.json({erro: error500 ? 'Erro interno do servidor. Entre em contato com a equipe de tecnologia.' : err.erro})
      }
    }

    app[verb](path, resolveRequest)
  })
}
