const crypto = require('crypto')

module.exports = (context) => Object.assign(context, {tokenGenerate: () => new Promise((resolve, reject) => {
  crypto.randomBytes(16, (err, buffer) => {
    if (err) return reject(err)
    const token = buffer.toString('hex')
    resolve(token)
  })
})})
