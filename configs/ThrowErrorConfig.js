module.exports = (context) => Object.assign(context, {throwError: (erro, statusCode = 400) => {
  const throwError = {erro, statusCode}
  throw throwError
}})
