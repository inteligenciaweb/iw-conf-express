const express = require('express')
const bodyParser = require('body-parser')
const expressPDF = require('express-pdf')
const compression = require('compression')

module.exports = (context) => {
  const limit = '850mb'
  const extended = true
  const app = express()

  app.use(compression())
  app.use(bodyParser.json({limit}))
  app.use(bodyParser.urlencoded({limit, extended}))
  app.use(expressPDF)

  Object.assign(context, {app})
}
